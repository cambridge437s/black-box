import csv
import heapq
from fuzzywuzzy import fuzz
from fuzzywuzzy import process
from multiprocessing import Pool

choices = ["Atlanta Falcons", "New York Jets", "New York Giants", "Dallas Cowboys", "Washington Redskins", "St. Louis Blues", "New York Yankees"]

def compare(tuple):
        return (100 - fuzz.ratio(tuple[0], tuple[1]), tuple[1], tuple[2])

def slowCompare(tuple):
        return (100 - fuzz.partial_ratio(tuple[0], tuple[1]), tuple[1], tuple[2])

#4
def fuzzySearchAuthors(author, n):
        authors = []

        authorsToISBN = {}

        with open('book32-listing 2.csv', encoding='mac_roman') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            for row in csv_reader:
                    if len(row[4]) == 0:
                            continue
                    tempAuth = row[4]
                    counter = 1
                    while tempAuth in authorsToISBN:
                        tempAuth = tempAuth + "_" + str(counter)
                        #print (tempAuth)
                    authors.append((author, row[4], tempAuth))
                    authorsToISBN[tempAuth] = row[0]
            p = Pool(100)
            authors = p.map(compare, authors)
            p.close()
            heapq.heapify(authors)

        booksReturned = []

        for i in range(n):
            auth = heapq.heappop(authors)
            print([auth[1], auth[2]])
            booksReturned.append(authorsToISBN[auth[2]])

        print(booksReturned)
        return booksReturned

#3
def fuzzySearchTitles(title, n):
        titles = []
        
        titlesToISBN = {}

        with open('book32-listing 2.csv', encoding='mac_roman') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            for row in csv_reader:
                    if len(row[3]) == 0:
                            continue
                    tempTitle = row[3]
                    counter = 1
                    while tempTitle in titlesToISBN:
                        tempTitle = tempTitle + "_" + str(counter)
                        #print (tempTitle)
                    titles.append((title, row[3], tempTitle))
                    titlesToISBN[tempTitle] = row[0]
            p = Pool(100)
            titles = p.map(compare, titles)
            p.close()
            heapq.heapify(titles)

        booksReturned = []

        for i in range(n):
            tit = heapq.heappop(titles)
            print([tit[1], tit[2]])
            booksReturned.append(titlesToISBN[heapq.heappop(titles)[2]])

        print(booksReturned)
        return booksReturned

def fuzzySearchTitlesGood(title, n):
        titles = []
        
        titlesToISBN = {}

        with open('book32-listing 2.csv', encoding='mac_roman') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            for row in csv_reader:
                    if len(row[3]) == 0:
                            continue
                    tempTitle = row[3]
                    counter = 1
                    while tempTitle in titlesToISBN:
                        tempTitle = tempTitle + "_" + str(counter)
                        #print (tempTitle)
                    titles.append((title, row[3], tempTitle))
                    titlesToISBN[tempTitle] = row[0]
            p = Pool(100)
            titles = p.map(slowCompare, titles)
            p.close()
            heapq.heapify(titles)

        booksReturned = []

        for i in range(n):
            tit = heapq.heappop(titles)
            print([tit[1], tit[2]])
            booksReturned.append(titlesToISBN[heapq.heappop(titles)[2]])

        print(booksReturned)
        return booksReturned

def fuzzySearchAuthorsGood(author, n):
        authors = []

        authorsToISBN = {}

        with open('book32-listing 2.csv', encoding='mac_roman') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            for row in csv_reader:
                    if len(row[4]) == 0:
                            continue
                    tempAuth = row[4]
                    counter = 1
                    while tempAuth in authorsToISBN:
                        tempAuth = tempAuth + "_" + str(counter)
                        #print (tempAuth)
                    authors.append((author, row[4], tempAuth))
                    authorsToISBN[tempAuth] = row[0]
            p = Pool(100)
            authors = p.map(slowCompare, authors)
            p.close()
            heapq.heapify(authors)

        booksReturned = []

        for i in range(n):
            auth = heapq.heappop(authors)
            print([auth[1], auth[2]])
            booksReturned.append(authorsToISBN[auth[2]])

        print(booksReturned)
        return booksReturned




#fuzzySearchTitles("Of Mice and Men", 5)
#fuzzySearchAuthors("JK", 5)
#fuzzySearchTitlesGood("Of Mice and Men", 5)
#fuzzySearchAuthorsGood("JK", 5)
