import csv
import heapq
import re
from fuzzywuzzy import fuzz
from fuzzywuzzy import process
from multiprocessing import Pool

choices = ["Atlanta Falcons", "New York Jets", "New York Giants", "Dallas Cowboys", "Washington Redskins", "St. Louis Blues", "New York Yankees"]

def compare(tuples, tuple):
    tuples.append((100 - fuzz.ratio(tuple[0].lower(), tuple[1].lower()), tuple[1], tuple[2]))
    return tuples

def slowCompare(tuples, tuple):
    tuples.append((100 - fuzz.partial_ratio(tuple[0].lower(), tuple[1].lower()), tuple[1], tuple[2]))
    return tuples

#4
def fuzzySearchAuthors(author, n):
        if n <= 0:
            return []
        author = re.sub('[;:\'\\")(*^%$#><?/!@#$,.]', '', author)
        authors = []

        authorsToBooks = {}

        with open('book32-listing 2.csv', encoding='mac_roman') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            for row in csv_reader:
                    auth = row[4]
                    if not auth in authorsToBooks:
                        authorsToBooks[auth] = []
                        authors.append((author, auth, auth))
                    authorsToBooks[auth].append(row)
            tuples = []
            for auth in authors:
                compare(tuples, auth)
            authors = tuples
            heapq.heapify(authors)

        booksReturned = []

        for i in range(len(authors)):
            auth = heapq.heappop(authors)
            print([auth[1], auth[2]])
            for book in authorsToBooks[auth[1]]:
                booksReturned.append(book)
                if len(booksReturned) >= n:
                    break
            if len(booksReturned) >= n:
                    break

        print(booksReturned)
        return booksReturned

#3
def fuzzySearchTitles(title, n):
        if n <= 0:
            return []
        titles = re.sub('[;:\'\\")(*^%$#><?/!@#$,.]', '', title)
        titles = []
        
        titlesToBooks = {}

        with open('book32-listing 2.csv', encoding='mac_roman') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            for row in csv_reader:
                    if len(row[3]) == 0:
                            continue
                    tempTitle = row[3]
                    tempTitle = re.sub('[;:\'\\")(*^%$#><?/!@#$,. ]', '', tempTitle)
                    counter = 1
                    while tempTitle in titlesToBooks:
                        tempTitle = tempTitle + "_" + str(counter)
                    titles.append((title, row[3], tempTitle))
                    titlesToBooks[tempTitle] = row
            tuples = []
            for tit in titles:
                compare(tuples, tit)
            titles = tuples
            heapq.heapify(titles)

        booksReturned = []

        for i in range(n):
            tit = heapq.heappop(titles)
            print([tit[1], tit[2]])
            booksReturned.append(titlesToBooks[tit[2]])

        print(booksReturned)
        return booksReturned

def fuzzySearchTitlesGood(title, n):
        if n <= 0:
            return []
        titles = re.sub('[;:\'\\")(*^%$#><?/!@#$,.]', '', title)
        titles = []
        
        titlesToBooks = {}

        with open('book32-listing 2.csv', encoding='mac_roman') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            for row in csv_reader:
                    if len(row[3]) == 0:
                            continue
                    tempTitle = row[3]
                    tempTitle = re.sub('[;:\'\\")(*^%$#><?/!@#$,. ]', '', tempTitle)
                    counter = 1
                    while tempTitle in titlesToBooks:
                        tempTitle = tempTitle + "_" + str(counter)
                    titles.append((title, row[3], tempTitle))
                    titlesToBooks[tempTitle] = row
            tuples = []
            for tit in titles:
                slowCompare(tuples, tit)
            titles = tuples
            heapq.heapify(titles)

        booksReturned = []

        for i in range(n):
            tit = heapq.heappop(titles)
            print([tit[1], tit[2]])
            booksReturned.append(titlesToBooks[tit[2]])

        print(booksReturned)
        return booksReturned

def fuzzySearchAuthorsGood(author, n):
        if n <= 0:
            return []
        author = re.sub('[;:\'\\")(*^%$#><?/!@#$,.]', '', author)
        authors = []

        authorsToBooks = {}

        with open('book32-listing 2.csv', encoding='mac_roman') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            for row in csv_reader:
                    auth = row[4]
                    if not auth in authorsToBooks:
                        authorsToBooks[auth] = []
                        authors.append((author, auth, auth))
                    authorsToBooks[auth].append(row)
            tuples = []
            for auth in authors:
                slowCompare(tuples, auth)
            authors = tuples
            heapq.heapify(authors)

        booksReturned = []

        for i in range(len(authors)):
            auth = heapq.heappop(authors)
            print([auth[1], auth[2]])
            for book in authorsToBooks[auth[1]]:
                booksReturned.append(book)
                if len(booksReturned) >= n:
                    break
            if len(booksReturned) >= n:
                    break

        print(booksReturned)
        return booksReturned

def getBookInfo(asin):
    books = {} 
    with open('book32-listing 2.csv', encoding='mac_roman') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            for row in csv_reader:
                    books[row[0]] = row
            return books[asin]




#fuzzySearchTitles("Of Mice and Men", 5)
#fuzzySearchAuthors("JK", 5)
