<?php
    //API URL
    $url = 'http://does.fyi:3456/rpc';

    //create a new cURL resource
    $ch = curl_init($url);

    //setup request to send json via POST
    $data = array(
        'author' => $_GET['author'],
        'numOfResults' => $_GET['numOfResults']
    );

    $stuff = array(
        "params" => $data,
        "method" => "Cambridge.searchAuthorsSlow",
        "id" => "1"
    );

    $payload = json_encode($stuff);

    //attach encoded JSON string to the POST fields
    curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);

    //set the content type to application/json
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

    //return response instead of outputting
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    //execute the POST request
    $result = curl_exec($ch);

    //close cURL resource
    curl_close($ch);


    $result = json_decode($result);

    foreach ($result->{'result'} as $book) {
        echo $book[0];
        echo "<br/>";

        echo $book[1];
        echo "<br/>";

        echo $book[2];
        echo "<br/>";

        // Title
        echo $book[3];
        echo "<br/>";

        // Author
        echo $book[4];
        echo "<br/>";

        echo $book[6];
        echo "<br/>";

        echo "<hr>";
    }

?>
