# Flask for py https://pypi.org/project/Flask/1.0.2/
# JSON-RPC for py https://pythonhosted.org/Flask-JSONRPC/
# Requests for py https://www.geeksforgeeks.org/get-post-requests-using-python/

# Google Books GET request https://www.googleapis.com/books/v1/volumes?q=isbn:<my-isbn>

from flask import Flask
from flask_jsonrpc import JSONRPC
import requests 
import subprocess
import sys
sys.path.insert(0, '../Search')
sys.path.insert(0, '../main/V2')
from fuzzySearch_Seq import *
from GetInfo import *

app = Flask(__name__)
jsonrpc = JSONRPC(app, '/rpc')


if __name__ == '__main__':
    app.run(host='0.0.0.0')

# Take this out to kill the front end
@app.route('/')
def hello():
    return "Hello, Dorian!"

@jsonrpc.method('Cambridge.searchTitles')
def searchTitles(title, numOfResults):
   return fuzzySearchTitles(title, int(numOfResults))

@jsonrpc.method('Cambridge.searchAuthors')
def searchAuthors(author, numOfResults):
   return fuzzySearchAuthors(author, int(numOfResults))

@jsonrpc.method('Cambridge.searchTitlesSlow')
def searchTitles(title, numOfResults):
    return fuzzySearchTitlesGood(title, int(numOfResults))

@jsonrpc.method('Cambridge.searchAuthorsSlow')
def searchAuthors(author, numOfResults):
   return fuzzySearchAuthorsGood(author, int(numOfResults))

@jsonrpc.method('Cambridge.getBookRecommendations')
def getBookRecommendations(acin, numOfResults):
    a = list()
    a.append("")
    a.append("recommendation") #the recommendation command
    a.append(acin) #the acin
    a.append(numOfResults) #the N to return
    return main(*a) #the call to the method

@jsonrpc.method('Cambridge.getBookFromAsin')
def getBookFromAsin(query):
    return getBookInfo(query)
