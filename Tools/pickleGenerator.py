import csv
import pickle

example_dict = {1:"6",2:"2",3:"f"}



with open('book32-listing 2.csv') as csv_file:
            authorToBook = {}
            titleToBook = {}
            asinToBook = {}

            csv_reader = csv.reader(csv_file, delimiter=',')
            for row in csv_reader:
                    author = row[4]
                    title = row[3]
                    asin = row[1][:len(row[1]) - 4]

                    if not author in authorToBook:
                        authorToBook[author] = []

                    authorToBook[author].append(row)
                    titleToBook[title] = row
                    asinToBook[asin] = row
            
            pickle_out = open("authorToBook.pickle","wb")
            pickle.dump(authorToBook, pickle_out)
            pickle_out.close()

            pickle_out = open("titleToBook.pickle","wb")
            pickle.dump(titleToBook, pickle_out)
            pickle_out.close()

            pickle_out = open("asinToBook.pickle","wb")
            pickle.dump(asinToBook, pickle_out)
            pickle_out.close()