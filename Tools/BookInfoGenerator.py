import csv
import requests 
import json

def getBookInfo(title):
    # API Keys
    # AIzaSyCa8erkTWWhZW-YWjYF2ITkygLFk5A09dk
    # AIzaSyDzZJqN3xvjHgEP0kquaC7O7Ds3CwS0S_A
    # AIzaSyASZdzT_bCxap2fqRNyo39eDYIgM42T_Zo
    # AIzaSyCdQtgd8Cu57e6hqLYwGhWkO2Gr8HerTLU
    # AIzaSyCfy8MFEUUjQDonFm_7WEEzGYlOGkH95ZA

    # URL to Google Books API
    URL = "https://www.googleapis.com/books/v1/volumes?q=" + title + ":keyes&key=AIzaSyCfy8MFEUUjQDonFm_7WEEzGYlOGkH95ZA"

    # no params needed
    PARAMS = {}

    r = requests.get(url = URL, params = PARAMS)


    try:
        # extracting data in json format 
        data = r.json() 

        book = {}

        if "items" in data.keys() and len(data["items"]) > 0:
            keys = data["items"][0]["volumeInfo"].keys()

            if "title" in keys:
                book["title"] = data["items"][0]["volumeInfo"]["title"]

            if "categories" in keys:
                book["categories"] = data["items"][0]["volumeInfo"]["categories"]

            if "authors" in keys:
                book["authors"] = data["items"][0]["volumeInfo"]["authors"]

            if "publisher" in keys:
                book["publisher"] = data["items"][0]["volumeInfo"]["publisher"]

            if "publishedDate" in keys:
                book["publishedDate"] = data["items"][0]["volumeInfo"]["publishedDate"]

            if "industryIdentifiers" in keys:
                book["industryIdentifiers"] = data["items"][0]["volumeInfo"]["industryIdentifiers"]

            if "imageLinks" in keys:
                book["imageLinks"] = data["items"][0]["volumeInfo"]["imageLinks"]

            if "textSnippet" in data["items"][0]["searchInfo"].keys():
                book["textSnippet"] = data["items"][0]["searchInfo"]["textSnippet"]

            if "averageRating" in keys:
                book["averageRating"] = data["items"][0]["volumeInfo"]["averageRating"]

        #return a list of recomended books
        return book
    except ValueError as e:
        return {}

def flushBooks(books, bookNumber):
    with open('bookInfo' + `bookNumber`  + '.txt', 'w') as csvfile:
        fieldnames = ['title', 'authors', 'categories', 'publisher', 'publishedDate', 'industryIdentifiers', 'imageLinks', 'textSnippet', 'averageRating', 'description']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames, delimiter='	')

        writer.writeheader()

        title = authors = categories = publisher = publishDate = industryIdentifiers = imageLinks = textSnippet = averageRating = ""

        for book in books:
            if "title" in book:
                title = book["title"].encode('utf-8')

            if "authors" in book:
                authors = book["authors"]

            if "categories" in book:
                categories = book["categories"]

            if "publisher" in book:
                publisher = book["publisher"].encode('utf-8')

            if "publishedDate" in book:
                publishedDate = book["publishedDate"].encode('utf-8')

            if "industryIdentifiers" in book:
                industryIdentifiers = book["industryIdentifiers"]

            if "imageLinks" in book:
                imageLinks = book["imageLinks"]

            if "textSnippet" in book:
                textSnippet = book["textSnippet"].encode('utf-8')

            if "averageRating" in book:
                averageRating = book["averageRating"]

            if "description" in book:
                description = book["description"]
            writer.writerow({'title': title, 'authors': authors, 'categories': categories, 'publisher': publisher, 'publishedDate': publishedDate, 'industryIdentifiers': industryIdentifiers, 'imageLinks': imageLinks, 'textSnippet': textSnippet, 'averageRating': averageRating, 'description': description})

with open('booksummaries.txt') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter='	')
    line_count = 0

    books = []

    for row in csv_reader:
        if (len(row[2]) == 0):
            line_count += 1
            continue
        if line_count != 0:
            line_count += 1
            continue

        book = getBookInfo(row[2])

        if len(row) >= 7 and len(book.keys()) > 0 and "industryIdentifiers" in book and len(book["industryIdentifiers"]) == 2 and "identifier" in book["industryIdentifiers"][0] and "identifier" in book["industryIdentifiers"][1] and (len(book["industryIdentifiers"][0]["identifier"]) == 10 or len(book["industryIdentifiers"][0]["identifier"]) == 13) and (len(book["industryIdentifiers"][1]["identifier"]) == 10 or len(book["industryIdentifiers"][1]["identifier"]) == 13):
            book["description"] = row[6]
            books.append(book)

            if "title" in book:
                print(`line_count` + " Title: " + book["title"])

        if line_count % 250 == 0:
            flushBooks(books, line_count)
            books = []

        line_count += 1

    flushBooks(books, line_count)

    print("\n\nProcessed " + `line_count` + " lines.")

    