from urlparse import urlparse
from threading import Thread
import httplib, sys
from Queue import Queue
import csv
import requests 

concurrent = 200

books = []

def doWork():
    while True:
        isbn = q.get()
        book = getBookInfo(isbn)
        if len(book.keys()) > 0:
            #books.append(book)
            print("Title: " + book["title"])
        q.task_done()

def getBookInfo(isbn):
    # URL to Google Books API
    URL = "https://www.googleapis.com/books/v1/volumes?q=isbn:" + isbn

    # no params needed
    PARAMS = {}

    r = requests.get(url = URL, params = PARAMS)

    # extracting data in json format 
    data = r.json() 

    r.connection.close()

    book = {}

    if "items" in data.keys() and len(data["items"]) > 0:
        keys = data["items"][0]["volumeInfo"].keys()

        if "title" in keys:
            book["title"] = data["items"][0]["volumeInfo"]["title"]

        if "authors" in keys:
            book["authors"] = data["items"][0]["volumeInfo"]["authors"]

        if "publisher" in keys:
            book["publisher"] = data["items"][0]["volumeInfo"]["publisher"]

        if "publishedDate" in keys:
            book["publishedDate"] = data["items"][0]["volumeInfo"]["publishedDate"]

        if "industryIdentifiers" in keys:
            book["industryIdentifiers"] = data["items"][0]["volumeInfo"]["industryIdentifiers"]

        if "imageLinks" in keys:
            book["imageLinks"] = data["items"][0]["volumeInfo"]["imageLinks"]

        if "textSnippet" in keys:
            book["textSnippet"] = data["items"][0]["searchInfo"]["textSnippet"]

        if "averageRating" in keys:
            book["averageRating"] = data["items"][0]["volumeInfo"]["averageRating"]

    #return a list of recomended books
    return book

q = Queue(concurrent * 2)
for i in range(concurrent):
    t = Thread(target=doWork)
    t.daemon = True
    t.start()
try:
    with open('BX-Books-Comma.csv') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0

        for row in csv_reader:
            if line_count != 0:
                if (len(row[0]) != 10 and len(row[0]) != 13):
                    print(row[0] + " is incorrect format for an ISBN")
                else:
                    q.put(row[0])
            line_count += 1
        q.join()
except KeyboardInterrupt:
    sys.exit(1)