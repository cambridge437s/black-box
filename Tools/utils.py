#Utils.py: this file contains helper functions that will be used by the book recommendation system - black box
#Author: Shane Tong

#The following function is used to return a given number of recommended books based on a list of lists
#The input will be a list of lists and an integer value x
#Each list in the list of lists represent one word's isbn/ids list. The word comes from the description of the current book and all values in the lists represents
#any other books that share the same word in their descriptions
#The integer value x represents how many most similar books we are trying to find for this current book
def findXSimilarBooks(wordList, x):
	#first create a dictionary that can be used to store number of times a given book has the same word in description as the current book
	bookOccurrence = {}
	#create a list to store the keys
	keys = []
	#count the number of occurrences for each books
	for curList in wordList:
		for book in curList:
			if book in bookOccurrence:
				bookOccurrence[book] += 1
			else:
				keys.append(book)
				bookOccurrence[book] = 1
	#sort the keys based on the occurences
	sortedList = sorted(keys, key=bookOccurrence.__getitem__)
	return [sortedList[i] for i in range(0,x)]