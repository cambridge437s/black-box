Major Changes Based On Sprints

Sprint 1 (Version 1.0.0):

	Initial Development of RPC
	
	Fetch Book Data

	Construct Book Datasets

Sprint 2 (Version 2.0.0):
	
	Initial Development of the Recommendation Algorithm

	Continuing Development of RPC

Sprint 3 (Version 2.1.0):
	
	Basic Working Recommendation Algorithm

Sprint 4 (Version 2.1.1):
	
	Integration of RPC and the Recommendation Algorithm

	Improvement on the Recommendation Algorithm

Sprint 5 (Version 3.0.0):

	Changing the Recommendation Algorithm to Use ASIN and Incorporate Titles and Genres to the Recommendation Algorithm
	
	Initial Development of Fuzzy Search

Sprint 6 (Version 4.0.0):
	
	Finishing the Development of Fuzzy Search

	Finishing the Integration of Project


