# Cambridge API

This repository is for development of WUSTL's Cambridge book recomendation API. This project aims to provide a highly personalized book recommendation and search API for a given dataset. 

## RPC
The RPC (Remote Procedure Call) requires the installation of the following packages to run properly.

- Flask for py https://pypi.org/project/Flask/1.0.2/
- JSON-RPC for py https://pythonhosted.org/Flask-JSONRPC/
- Requests for py https://www.geeksforgeeks.org/get-post-requests-using-python/
- FuzzyWuzzy for py https://github.com/seatgeek/fuzzywuzzy

Once these have been installed, you can run the RPC on your local machine by executing the following command:

`FLASK_APP=RPCEndpoint.py flask run`

or 

`sudo env 'PATH=$PATH:/usr/local/bin' FLASK_APP=RPCEndpoint.py flask run -p 3456 --host=0.0.0.0`

on an EC2 instance.

In another terminal session, you can send and receive requests from the RPC by running variants of the following command:

`curl -i -X POST -H "Content-Type: application/json; indent=4" -d 
'{ "jsonrpc": "2.0", 
"method": "Cambridge.getBookRecommendations", 
"params": {"acin": "1507653395", "numOfResults":"6"}, "id": "1" }' http://does.fyi:3456/rpc`

Notice that the method we are hitting is "Cambridge.getBookRecommendations". This takes the params "acin" and "numOfResults". Currently, there are 4 methods available to hit with the names and parameters that follow....

| Method                           | Input                | Output          |
|----------------------------------|----------------------|-----------------|
| Cambridge.searchTitles           | title, numOfResults  | A list of books |
| Cambridge.searchTitlesSlow       | title, numOfResults  | A list of books |
| Cambridge.searchAuthors          | author, numOfResults | A list of books |
| Cambridge.searchAuthorsSlow      | author, numOfResults | A list of books |
| Cambridge.getBookRecommendations | acin, numOfResults   | A list of books |
| Cambridge.getBookFromAsin        | query                | A book          |

