
# coding: utf-8

# There are three pipelines:  
# 1. Setting up Pipeline: Importing all books and getting special words  
# 2. Add Book Pipeline: Adding a book to the word-id dictionary so that it can be found in future requests  
# 3. Get Recommendation Pipeline: Uses the word-id dictionary to find the most similar books 
# 
# This whole notebook is divided into functional blocks. each one either contains a method or the pipeline itself that calls those methods
# 

# In[ ]:


#READ in books
#PART OF SETTING UP PIPELINE
#reads in the files from a directory and return descriptions array, 
#titles array, and isbns array
def ReadInBooks(directory):
    import glob
    import csv
    import nltk

    descriptions = []
    titles = []
    isbns = []
    for filename in glob.glob(directory):
        with open(filename, 'r',errors='ignore') as f:
            reader = csv.reader(f, delimiter='\t')
            firstLine = True
            for row in reader:            
                if(not firstLine):
                    foundAUid = False
                    uids = row[5].split("u'")
                    for i in range(len(uids)):
                        if uids[i] == "ISBN_13'}, {":
                            u = uids[i-2][:-3]
                            isbns.append("13_"+u)
                            foundAUid = True
                            break
                        elif uids[i] == "ISBN_10'}, {":
                            u = uids[i-2][:-3]
                            isbns.append("10_" + u)
                            foundAUid = True
                    if(foundAUid):
                        descriptions.append(row[-1])
                        titles.append(row[0])
                else:
                    firstLine = False
                    '''['title', 'authors', 'publisher', 'publishedDate', 'industryIdentifiers', 'imageLinks', 'textSnippet', 'averageRating', 'description']'''
    return descriptions, titles, isbns


# In[ ]:


#Get TFIDF_vals
#Get important words list from descriptions
#PART OF SETTING UP PIPELINE
#returns the sortedwordscores which is a list of the words and their scores as tuples
def getTFIDF_Vals(descriptions):
    from sklearn.feature_extraction.text import TfidfVectorizer
    tfidf = TfidfVectorizer(stop_words='english');
    response = tfidf.fit_transform(descriptions)
    feature_names = tfidf.get_feature_names()
    
    wordScoresDict = dict()
    wordScores = []
    for col in response.nonzero()[1]:
        if(response[0,col]!= 0):
            wordScoresDict[feature_names[col]] = response[0, col]

    for key, value in wordScoresDict.items():
        wordScores.append((key,value))
    
    sortedWordScores = sorted(wordScores, key=lambda tup: tup[1],reverse = True)
    
    return sortedWordScores


# In[ ]:


#Set wordtoid
#Sets the word-id dictionary from the sortedwordscroes
#PART OF SETTING UP PIPELINE
#returns the wordtoId dict

def setWordToIdDict(descriptions, sortedWordScores):
    wordToId = dict()
    for num in range(len(descriptions)): #for all books
        for word in sortedWordScores: #for all flagged words
            if word[0] in descriptions[num]: #if the word is in the book description
                if word[0] in wordToId:
                    w = wordToId[word[0]]
                    w.append(num)
                    wordToId[word[0]] = w
                else:
                    l = []
                    l.append(num)
                    wordToId[word[0]] = l
    return wordToId


# In[ ]:


#used for saving any objects to files 
def saveElement(element, location):
    import pickle
    pickle_out = open(location,"wb")
    pickle.dump(element, pickle_out)
    pickle_out.close()


# In[5]:


#used for loading any objects from files 
def loadElement(location):
    import pickle
    pickle_in = open(location,"rb")
    return pickle.load(pickle_in)


# In[ ]:


#----------SETTING UP PIPELINE MAIN BLOCK-------------

def runWordFiles(in_dir, out_dir):
    #read in everything from raw files
    descriptions, titles, isbns = ReadInBooks(in_directory)
    #get important words
    wordScores = getTFIDF_Vals(descriptions)
    #turn all books into word arrays
    wordToID = setWordToIdDict(descriptions,wordScores)
    #save the files
    saveElement(descriptions,out_directory+"desc.pickle")
    saveElement(titles,out_directory+"titl.pickle")
    saveElement(isbns,out_directory+"isbn.pickle")
    saveElement(wordScores,out_directory+"wordSc.pickle")
    saveElement(wordToID,out_directory+"wordID.pickle")


#check to see if you need to run the word files
import sys
in_directory = r'/home/maj/black-box/Tools/*.txt'
out_directory = r'C:/Users/Christian/Desktop/ClassWork/SoftwareDev/The Repository/black-box/WordFiles'


if(len(sys.argv)> 1):
    if(sys.argv[1] == "y"):
        print("Creating Word Files")
        runWordFiles(in_directory,out_directory)
        print("Word Files Created")
        sys.exit()
else:
    print("Invalid Arguments")
    sys.exit()
#----------SETTING UP PIPELINE MAIN BLOCK-------------


# In[ ]:


#add book to wordtoid dictionary
#adds a book to the the word-id dictionary using the sortedwordscroes, word-id, the book description, and id label
#PART OF ADD BOOK PIPELINE
#returns nothing
def addBookToWordToID(wordToID, sortedWordScores, bookDescription, bookId):
    for word in sortedWordScores: #for all flagged words
        if word[0] in bookDescription: #if the word is in the book description
            if word[0] in wordToId:
                w = wordToId[word[0]]
                w.append(num)
                wordToId[word[0]] = w
            else:
                l = []
                l.append(num)
                wordToId[word[0]] = l
    return wordToID


# In[ ]:


#----------Add BOOK PIPELINE MAIN BLOCK-------------


#----------Add BOOK PIPELINE MAIN BLOCK-------------


# In[4]:


#loads all the important elements needed to give a recommendation
#part of the get recommendation pipeline

def loadAllImportantElements(location):
    descriptions = loadElement(out_directory+"desc.pickle")
    titles = loadElement(out_directory+"titl.pickle")
    isbns = loadElement(out_directory+"isbn.pickle")
    wordScores = loadElement(out_directory+"wordSc.pickle")
    wordToID = loadElement(out_directory+"wordID.pickle")
    return descriptions,titles,isbns,wordScores,wordToID
    


# In[3]:


#get top value from a list and remove it
#used to get top values from the list of similar bookds
#PART OF ADD BOOK PIPELINE
#returns nothing
def getTopFromListAndRemove(freq,idSet):
    maxFreq = max(freq)
    maxFreqId = idSet[freq.index(max(freq))]
    locFreq = freq.index(max(freq))
    freq[locFreq] = -1
    idSet[locFreq] = -1
    
    return maxFreq,maxFreqId,freq,idSet


# In[24]:
def findXSimilarBooks(wordList, x, descNum,titles, isbns, descriptions):
    #first create a dictionary that can be used to store number of times a given book has the same word in description as the current book
    bookOccurrence = {}
    #create a list to store the keys
    keys = []
    #count the number of occurrences for each books
    for curList in wordList:
        for book in curList:
            if book in bookOccurrence:
                bookOccurrence[book] += 1
            elif book != descNum:
                keys.append(book)
                bookOccurrence[book] = 1
    #sort the keys based on the occurences
    print(len(keys))
    sortedList = sorted(keys, key=bookOccurrence.__getitem__, reverse=True)
    return [(sortedList[i],titles[sortedList[i]],isbns[sortedList[i]],descriptions[sortedList[i]]) for i in range(0,x)]


def findMostSimilarBooks(description,descNum,numSimilarToReturn,sortedWordScores,descriptions,titles,isbns,wordToId):
    wordsInCommon = []
    for word in sortedWordScores:
        if word[0] in description:
            wordsInCommon.append(word[0]) 
            
    superList = []
    for word in wordsInCommon:
        superList.append(wordToId[word])

    return findXSimilarBooks(superList, numSimilarToReturn, descNum, titles, isbns, descriptions)


# In[25]:


#----------GET RECOMMENDATION PIPELINE MAIN BLOCK-------------
def getRecommendation(out_dir, description, descNum, numSimilarToReturn):
    descriptions, titles, isbns,wordScores,wordToID = loadAllImportantElements(out_directory)
    print(titles[descNum])    
    similarBooks = findMostSimilarBooks(description,descNum,numSimilarToReturn,wordScores,descriptions,titles,isbns,wordToID)
    
    return similarBooks

#----------GET RECOMMENDATION PIPELINE MAIN BLOCK-------------


# In[27]:


#testing the recommendation system
import time
start = time.time()
descriptionNum = -1
count = 3
if(len(sys.argv) > 1):
    #print("Getting Book Recommendation Files")
    descriptionNum = int(sys.argv[1])

    if(len(sys.argv) > 2):
        #print("With Specific Count")
        count = int(sys.argv[2])

if(descriptionNum != -1): 
    similar  = getRecommendation(out_directory, loadElement(out_directory+"desc.pickle")[descriptionNum],descriptionNum,count)

    titles = loadElement(out_directory+"titl.pickle")
    for x in range(100):
        print(x,titles[x])
		
    for i in similar:
        print(i[2],i[1],i[0])
end = time.time()
print("time: " + str(end-start))

