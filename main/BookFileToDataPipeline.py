#BooksFromDataPipeline


# coding: utf-8

# There are three pipelines:  
# 1. Setting up Pipeline: Importing all books and getting special words  
# 2. Add Book Pipeline: Adding a book to the word-id dictionary so that it can be found in future requests  
# 3. Get Recommendation Pipeline: Uses the word-id dictionary to find the most similar books 
# 
# This whole notebook is divided into functional blocks. each one either contains a method or the pipeline itself that calls those methods
# 

# In[ ]:


#READ in books
#PART OF SETTING UP PIPELINE
#reads in the files from a directory and return descriptions array, 
#titles array, and isbns array
def ReadInBooks(directory):
    import glob
    import csv
    import nltk

    descriptions = []
    titles = []
    isbns = []
    for filename in glob.glob(directory):
        with open(filename, 'r',errors='ignore') as f:
            reader = csv.reader(f, delimiter='/t')
            firstLine = True
            for row in reader:            
                if(not firstLine):
                    foundAUid = False
                    uids = row[5].split("u'")
                    for i in range(len(uids)):
                        if uids[i] == "ISBN_13'}, {":
                            u = uids[i-2][:-3]
                            isbns.append("13_"+u)
                            foundAUid = True
                            break
                        elif uids[i] == "ISBN_10'}, {":
                            u = uids[i-2][:-3]
                            isbns.append("10_" + u)
                            foundAUid = True
                    if(foundAUid):
                        descriptions.append(row[-1])
                        titles.append(row[0])
                else:
                    firstLine = False
                    '''['title', 'authors', 'publisher', 'publishedDate', 'industryIdentifiers', 'imageLinks', 'textSnippet', 'averageRating', 'description']'''
    return descriptions, titles, isbns


# In[ ]:


#Get TFIDF_vals
#Get important words list from descriptions
#PART OF SETTING UP PIPELINE
#returns the sortedwordscores which is a list of the words and their scores as tuples
def getTFIDF_Vals(descriptions):
    from sklearn.feature_extraction.text import TfidfVectorizer
    tfidf = TfidfVectorizer(stop_words='english');
    response = tfidf.fit_transform(descriptions)
    feature_names = tfidf.get_feature_names()
    
    wordScoresDict = dict()
    wordScores = []
    for col in response.nonzero()[1]:
        if(response[0,col]!= 0):
            wordScoresDict[feature_names[col]] = response[0, col]

    for key, value in wordScoresDict.items():
        wordScores.append((key,value))
    
    sortedWordScores = sorted(wordScores, key=lambda tup: tup[1],reverse = True)
    
    return sortedWordScores


# In[ ]:


#Set wordtoid
#Sets the word-id dictionary from the sortedwordscroes
#PART OF SETTING UP PIPELINE
#returns the wordtoId dict

def setWordToIdDict(descriptions, sortedWordScores):
    wordToId = dict()
    for num in range(len(descriptions)): #for all books
        for word in sortedWordScores: #for all flagged words
            if word[0] in descriptions[num]: #if the word is in the book description
                if word[0] in wordToId:
                    w = wordToId[word[0]]
                    w.append(num)
                    wordToId[word[0]] = w
                else:
                    l = []
                    l.append(num)
                    wordToId[word[0]] = l
    return wordToId


#used for saving any objects to files 
def saveElement(element, location):
    import pickle
    pickle_out = open(location,"wb")
    pickle.dump(element, pickle_out)
    pickle_out.close()


#----------SETTING UP PIPELINE MAIN BLOCK-------------

def runWordFiles(in_dir, out_dir):
    #read in everything from raw files
    descriptions, titles, isbns = ReadInBooks(in_directory)
    #get important words
    wordScores = getTFIDF_Vals(descriptions)
    #turn all books into word arrays
    wordToID = setWordToIdDict(descriptions,wordScores)
    #save the files
    saveElement(descriptions,out_directory+"desc.pickle")
    saveElement(titles,out_directory+"titl.pickle")
    saveElement(isbns,out_directory+"isbn.pickle")
    saveElement(wordScores,out_directory+"wordSc.pickle")
    saveElement(wordToID,out_directory+"wordID.pickle")


#check to see if you need to run the word files
import sys
in_directory = r'/home/maj/black-box/Book_Info/*.txt'
out_directory = r'/home/maj/black-box/WordFiles/'


print("Creating Word Files")
runWordFiles(in_directory,out_directory)
print("Word Files Created")
sys.exit()
