
#used for loading any objects from files 
def loadElement(location):
    import pickle
    pickle_in = open(location,"rb")
    return pickle.load(pickle_in)


#loads all the important elements needed to give a recommendation
#part of the get recommendation pipeline

def loadAllImportantElements(location):
    descriptions = loadElement(out_directory+"desc.pickle")
    titles = loadElement(out_directory+"titl.pickle")
    isbns = loadElement(out_directory+"isbn.pickle")
    wordScores = loadElement(out_directory+"wordSc.pickle")
    wordToID = loadElement(out_directory+"wordID.pickle")
    return descriptions,titles,isbns,wordScores,wordToID
    
def loadToLocationFromMongo(location):
    

# In[3]:


#get top value from a list and remove it
#used to get top values from the list of similar bookds
#PART OF ADD BOOK PIPELINE
#returns nothing 
def getTopFromListAndRemove(freq,idSet):
    maxFreq = max(freq)
    maxFreqId = idSet[freq.index(max(freq))]
    locFreq = freq.index(max(freq))
    freq[locFreq] = -1
    idSet[locFreq] = -1
    
    return maxFreq,maxFreqId,freq,idSet


# In[24]:
def findXSimilarBooks(wordList, x, descNum,titles, isbns, descriptions):
    #first create a dictionary that can be used to store number of times a given book has the same word in description as the current book
    bookOccurrence = {}
    #create a list to store the keys
    keys = []
    #count the number of occurrences for each books
    for curList in wordList:
        for book in curList:
            if book in bookOccurrence:
                bookOccurrence[book] += 1
            elif book != descNum:
                keys.append(book)
                bookOccurrence[book] = 1
    #sort the keys based on the occurences
    print(len(keys))
    sortedList = sorted(keys, key=bookOccurrence.__getitem__, reverse=True)
    return [(sortedList[i],titles[sortedList[i]],isbns[sortedList[i]],descriptions[sortedList[i]]) for i in range(0,x)]


def findMostSimilarBooks(description,descNum,numSimilarToReturn,sortedWordScores,descriptions,titles,isbns,wordToId):
    wordsInCommon = []
    for word in sortedWordScores:
        if word[0] in description:
            wordsInCommon.append(word[0]) 
            
    superList = []
    for word in wordsInCommon:
        superList.append(wordToId[word])

    return findXSimilarBooks(superList, numSimilarToReturn, descNum, titles, isbns, descriptions)


# In[25]:


#----------GET RECOMMENDATION PIPELINE MAIN BLOCK-------------
def getRecommendation(out_dir, description, descNum, numSimilarToReturn):
    descriptions, titles, isbns,wordScores,wordToID = loadAllImportantElements(out_directory)
    print(titles[descNum])    
    similarBooks = findMostSimilarBooks(description,descNum,numSimilarToReturn,wordScores,descriptions,titles,isbns,wordToID)
    
    return similarBooks

#----------GET RECOMMENDATION PIPELINE MAIN BLOCK-------------


# In[27]:

in_directory = r'/home/maj/black-box/Book_Info/*.txt'
out_directory = r'C:/Users/Christian/Desktop/ClassWork/SoftwareDev/The Repository/black-box/WordFiles'


#testing the recommendation system
import time
import sys
start = time.time()
descriptionNum = -1
count = 3
if(len(sys.argv) > 1):
    #print("Getting Book Recommendation Files")
    descriptionNum = int(sys.argv[1])

    if(len(sys.argv) > 2):
        #print("With Specific Count")
        count = int(sys.argv[2])

if(descriptionNum != -1): 
    similar  = getRecommendation(out_directory, loadElement(out_directory+"desc.pickle")[descriptionNum],descriptionNum,count)

    titles = loadElement(out_directory+"titl.pickle")
    for x in range(100):
        print(x,titles[x])
		
    for i in similar:
        print(i[2],i[1],i[0])
end = time.time()
print("time: " + str(end-start))

