import pymongo

#https://www.w3schools.com/python/python_mongodb_getstarted.asp

#start the connection with mongo atalas
myclient = pymongo.MongoClient("mongodb+srv://maj:12345678910@cluster437-bioqs.mongodb.net/test?retryWrites=true")
mydb = myclient['db437mongo']

#use the collection books in db 'db437mongo'
mycol = mydb["books"]


#sample insertion
mybook = { "isbn": "123123123", "title": "Foo", "author": "maj"}

#sample insertion method
x = mycol.insert_one(mybook)

#deletion
y = mycol.delete_one(mybook)

#some insertion
mybooks = [mybook, { "isbn": "123123124", "title": "Foo", "author": "maj"}]
v = mycol.insert_many(mybooks)
#find
mynew = {"author":"maj"}
mydoc = mycol.find(mynew)

for doc in mydoc:
    print(doc.get('_id'))
#sort: mydoc.sort("author", pymongo.ASCENDING)
q = mydoc.count()

#multiple deletion
#https://docs.mongodb.com/manual/reference/method/db.collection.deleteMany/
#z = mycol.delete_many(mynew)

print (x)
print (y)

print(v.inserted_ids)


print(q, " found.")


#print (z.deleted_count, " deleted.")
