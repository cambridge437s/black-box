#used for saving any objects to files 
def saveElement(element, location):
    import pickle
    pickle_out = open(location,"wb")
    pickle.dump(element, pickle_out)
    pickle_out.close()

#used for loading any objects from files 
def loadElement(location):
    import pickle
    pickle_in = open(location,"rb")
    return pickle.load(pickle_in)

import csv
repoFolder = r'C:/Users/Christian/Desktop/ClassWork/SoftwareDev/The Repository/black-box/main/V2/'


def readInCsv():
    bookFileLoc = r'C:/Users/Christian/Desktop/ClassWork/SoftwareDev/The Repository/black-box/Tools/book32-listing 2.csv'
    rows = list()

    #Loading the book csv
    with open(bookFileLoc,encoding="utf8",errors='ignore') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            rows.append(row)
            line_count+=1   

    print(f'Processed {line_count} lines.')
    return rows

rows = readInCsv()

acinLoc = 0
jpgLoc = 1
picURLLoc = 2
titleLoc = 3
authorLoc = 4
genreNumLoc = 5
genreDescLOc = 6

#set the value to 1 to generate that file, 0 is testing 
doWork = [0,0,0,0,0,1]


#testing
if(doWork[0]):
    import time
    start = time.time()
    mapping = loadElement(repoFolder+"/Pickles/wordMapping.pkl")
    counts = dict()

    for word in mapping.keys():
        if(len(word) in counts):
            counts[len(word)]+=1
        else:
            counts[len(word)] = 1
    for num in counts.keys():
        print(num,',',counts[num])
    end = time.time()
    print(end-start)


if(doWork[1]):
    #Authors
    authorDict = dict()

    for row in rows:
        if(row[authorLoc] in authorDict):
            a = authorDict[row[authorLoc]]
            a.append((row[acinLoc],row[titleLoc]))
            authorDict[row[authorLoc]] = a
        else:
            a = list()
            a.append((row[acinLoc],row[titleLoc]))
            authorDict[row[authorLoc]] = a


    saveElement(authorDict,repoFolder + "Pickles/author.pkl")
    print("Saved Author File")

if(doWork[2]):
    #Title
    titleDict = dict()

    for row in rows:
        if(row[titleLoc] in titleDict):
            titleDict[row[titleLoc]] = (row[acinLoc],row[authorLoc])
        else:
            titleDict[row[titleLoc]] = (row[acinLoc],row[authorLoc])

    saveElement(titleDict,repoFolder + "Pickles/title.pkl")
    print("Saved Title File")

if(doWork[3]):
    #ACIN
    acinDict = dict()

    for row in rows:
        if(row[acinLoc] in acinDict):
            print("Shouldnt see this")
        else:
            acinDict[row[acinLoc]] = (row[titleLoc],row[authorLoc],row[picURLLoc],row[genreNumLoc])
    saveElement(acinDict,repoFolder + "Pickles/acin.pkl")
    print("Saved ACIN File")
if(doWork[4]):
    #Word mappings
    from nltk.tokenize import word_tokenize
    mapping = dict()

    for row in rows:
        words = word_tokenize(row[titleLoc])

        for w in words:
            if(w in mapping):
                a = mapping[w]
                a.append(row[0])
                mapping[w] = a
            else:
                a = list()
                a.append(row[0])
                mapping[w] = a
    #finding bad keys
    removeList = list()
    for word in mapping.keys():
        if(len(mapping[word]) == 1):
            removeList.append(word)
        #elif(len(mapping[word]) > 20):
           # removeList.append(word)
    #removing bad keys
    for word in removeList:
        mapping.pop(word)
    saveElement(mapping,repoFolder + "Pickles/wordMapping.pkl")
    print("Saved Word Mapping File")
if(doWork[5]):
    #genre stuff

    genreDict = dict()

    for row in rows:
        if(row[genreNumLoc] in genreDict):
            a = genreDict[row[genreNumLoc]]
            if len(a) < 100:
                a.append(row[acinLoc])
                genreDict[row[genreNumLoc]] = a
        else:
            a = list()
            a.append(row[acinLoc])
            genreDict[row[genreNumLoc]] = a

    saveElement(genreDict,repoFolder + "Pickles/genre.pkl")
    print("Saved Genre File")
    genreDict.clear()

    for row in rows:
        genreDict[row[genreNumLoc]] = row[genreDescLOc]
    saveElement(genreDict,repoFolder + "Pickles/genreDesc.pkl")
    print("Saved Genre File")