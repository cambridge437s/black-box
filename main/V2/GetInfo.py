
#-------------------Super important variable
repoFolder = r'/home/maj/black-box/main/V2'



def getTitle(title,location):
    titles = loadElement(location)
    return(titles[title])
    
def getAuthor(author,location):
    authors = loadElement(location)
    return(authors[author])

#used for loading any objects from files 
def loadElement(location):
    import pickle
    pickle_in = open(location,"rb")
    return pickle.load(pickle_in)

def getTitleWordRecommendation(acin, title, wordMappingLoc,N):
    from nltk.tokenize import word_tokenize
    words = word_tokenize(title)
    
    mapping = loadElement(wordMappingLoc)

    bookOccurrence = {}
    keys = []

    for word in words:
        if word in mapping:
            for book in mapping[word]:
                if book in bookOccurrence:
                    bookOccurrence[book] += 1
                else:
                    keys.append(book)
                    bookOccurrence[book] = 1

    sortedList = sorted(keys, key=bookOccurrence.__getitem__, reverse=True)
    
    if(len(sortedList)> N):
        if(acin in sortedList[0:N]):
            sortedList.remove(acin)
        else:
            return sortedList[0:N]
    else:
        if(acin in sortedList):
            sortedList.remove(acin)
        return sortedList

        


def getRecommendation(acin, N, wordMappingLoc,authorLoc,acinLoc,genreLoc):
    recommendations = list()

    #loading in acins and authors
    authors = loadElement(authorLoc)
    acins = loadElement(acinLoc)

    #getting the authors books
    authorBooks = authors[acins[acin][1]]

    for book in authorBooks:
        if(len(recommendations) < N):
            if(acin != book[0]):
                recommendations.append((book[0],*acins[book[0]]))
        else:
            return recommendations

    wordRecs = getTitleWordRecommendation(acin,acins[acin][0],wordMappingLoc, N-len(recommendations))
    if(wordRecs):
        if(len(recommendations) < N):
            for w in wordRecs:
                if(acin != w):
                    recommendations.append((w,*acins[w]))
        else:
            return recommendations
    #TODO add in random thing in the same genre thing
    
    genreDescNum = acins[acin][-1]
    genreRand = loadElement(genreLoc)

    for i in genreRand[genreDescNum]:
        if(len(recommendations) < N):
            if(acin != i):
                recommendations.append((i,*acins[i]))
        else:
            return recommendations

    return recommendations

import time
def main(*argv):
    start = time.time()
    argv = list(argv)
    if(len(argv) > 1):
        #author
        if(argv[1] == 'author'):
            if(len(argv) == 4):
                try:
                    a = getAuthor(argv[2],repoFolder +"/Pickles/author.pkl")
                    N = int(argv[3])
                    count = 0
                    for i in range(0,len(a)):
                        count+=1
                        if(count <= N):
                            print(a[i])
                        else:
                            break
                except:
                    print("Invalid Author Name")
                
                
            else:
                print('Run Incorrectly: Expecting \'Author\' and \'N\'')
        
        #title
        elif(argv[1] == 'title'):
            if(len(argv) == 3):
                try:
                    a = getTitle(argv[2],repoFolder +"/Pickles/title.pkl")
                    print(a)
                except:
                    print("Invalid Title Name")
            else:
                print('Run Incorrectly: Expecting \'Title\'')
        elif(argv[1] == 'acin'):
            if(len(argv) == 3):
                try:
                    
                    a = getTitle(argv[2],repoFolder +"/Pickles/acin.pkl")
                    
                    genreDesc = loadElement(repoFolder +"/Pickles/genreDesc.pkl")
                    c = list(a[:-1])
                    c.append(genreDesc[a[-1]])
                    print(c)
                except:
                    print("Invalid Acin")
            else:
                print('Run Incorrectly: Expecting \'ACIN\'')
        elif(argv[1] == 'recommendation'):
            if(len(argv) == 4):
                    books = []
                #try:
                    N = int(argv[3])
                    a = getRecommendation(argv[2],N,repoFolder +"/Pickles/wordMapping.pkl",repoFolder +"/Pickles/author.pkl",repoFolder +"/Pickles/acin.pkl",repoFolder +"/Pickles/genre.pkl")

                    genreDesc = loadElement(repoFolder +"/Pickles/genreDesc.pkl")
                    for v in a:
                        c = list(v[:-1])
                        c.append(genreDesc[v[-1]])
                        print(c)
                        books.append(c)
                #except:
                    #print("Invalid Acin")
                    return books
            else:
                print('Run Incorrectly: Expecting \'ACIN\' and \'N\'')
        else:
            print('Run Incorrectly: Invalid Info type')
        
    else:
        print("Invalid Info Description, not enough values entered")
    endTime = time.time()
    print('Finish time:',endTime-start)


#ACTUAL RUNNER from cmd line
import sys

if __name__ == "__main__":
    main(*sys.argv)
    #interesting examples 
    # 1507653395
    # 1491918071